
@extends('layouts.app')


@section('title')
 index - todos
@endsection

@section('content')


<div class="container">
<div class="row pt-3 justify-content-center">
<div class="card" style="width:50%">
<div class="card-header text-center">
<h1>All todos</h1>
<span class="float-right"><a href="/create" > <i class="fal fa-plus-circle mr-2" style="color: #0b0f8a"></i></a> </span>

</div>

<div class="card-body">
@if (session()->has('success'))
   <div class="alert alert-success">
   {{session()->get('success')}}
   </div>

@endif
<ul class="list-group">
   @forelse($todos as $todo)
        <li class="list-group-item" >{{$todo->title}}
        
       
        <span class="float-right"> <a href="/todos/{{$todo->id}}/delete" > <i class="fa fa-trash-o mr-2"  style="color: #f17979"
></i></a> </span>
        <span class="float-right"><a href="todos/{{$todo->id}}" > <i class="fa fa-eye mr-2" style="color: #189477"></i></a> </span> 
        <span class="float-right"><a href="todos/{{$todo->id}}/edit" > <i class="fa fa-pencil-square-o mr-2" style="color: #0b0f8a"></i></a> </span>
         <span class="float-right"> 
         
         @if(! $todo->completed)
         <a href="todos/{{$todo->id}}/complete" > 
         @else 
         <a href="todos/{{$todo->id}}/uncomplete" > 
         @endif
         

         
         @if(! $todo->completed )
         <i class="fa fa-check mr-2" style="color: #c1baba">
         @else
         <i class="fa fa-check mr-2"  style="color: #333333">
         @endif
</i></a> </span>

         </li> 
         @empty
    <p>No todos</p>
        @endforelse
</ul>
</div>
</div>
</div>
</div>

@endsection
