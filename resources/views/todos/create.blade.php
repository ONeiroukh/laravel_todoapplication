


@extends('layouts.app')


@section('title')
 create todos
@endsection

@section('content')

<div class="container pt-5">
<div class="row justify-content-center mt-5">
<div class="col-md-6">
<div class="card">
<div class="card-header">
<h1>create a new todo</h1>
</div>
<div class="card-body">
<!-- @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif -->
<form action="/create" method="post">
@csrf 
  <div class="form-group">
       <input type="text" class="form-control" class=" @error('todoName') is-invalid @enderror"
        name="todoName"  value="{{old('todoName')}}" placeholder="Type todo name">
     </div>
     @error('todoName')
    <div class="alert alert-danger">{{ $message }}</div>
     @enderror
     <div class="form-group">
     <textarea class="form-control "  class="@error('todoDescription') is-invalid @enderror"
     name="todoDescription"    placeholder="Type description " rows="3">{{old('todoDescription')}}</textarea>
     </div>
     @error('todoDescription')
    <div class="alert alert-danger">{{ $message }}</div>
     @enderror
     <div class="formgroup text-center" >
     <button type="submit" class="btn btn-success " style="width:40%"> Create todo </button>
     </div>
  </form>
</div>
</div>
</div>
</div>
</div>



 @endsection

