


@extends('layouts.app')


@section('title')
 update todos
@endsection

@section('content')

<div class="container pt-5">
<div class="row justify-content-center mt-5">
<div class="col-md-6">
<div class="card">
<div class="card-header">
<h1>update todo</h1>
</div>
<div class="card-body">
<!-- @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif -->
<form action="/todos/{{$todo->id}}" method="post">
@csrf 
  <div class="form-group">
       <input type="text" class="form-control" class=" @error('todoName') is-invalid @enderror"
        name="todoName"  value="{{$todo->title}}" placeholder="Type todo name">
     </div>
     @error('todoName')
    <div class="alert alert-danger">{{ $message }}</div>
     @enderror
     <div class="form-group">
     <textarea class="form-control "  class="@error('todoDescription') is-invalid @enderror"
     name="todoDescription"   placeholder="Type description " rows="3">{{$todo->description}}</textarea>
     </div>
     @error('todoDescription')
    <div class="alert alert-danger">{{ $message }}</div>
     @enderror
     <div class="formgroup text-center" >
     <button type="submit" class="btn btn-success " style="width:40%"> Update </button>
     </div>
  </form>
</div>
</div>
</div>
</div>
</div>



 @endsection

