<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todo;

class todosController extends Controller
{
    //
    public function index() {
        
        $todos=Todo::all();

        return view('todos.index',compact('todos'));
    }

    public function show(Todo $todo) {
        //$todo=Todo::find($todo);
        return view('todos.show',compact('todo'));

    }

    public function create() {
        
        return view('todos.create');
    }

    public function store(Request $request) {
        
        //return $request->todoName;
        
      /*   $request->validate([
            'todoName'=>'required|min:6',
            'todoDescription'=>'required'
        ]);
 */

$this->validate($request,[
    'todoName'=>'required|min:6',
    'todoDescription'=>'required'
]);

        $todo= new Todo;
        $todo->title=$request->todoName;
        $todo->description=$request->todoDescription;

        $todo->save();

        $request->session()->flash('success' ,'todo created successfuly');

        return redirect('/todos');
    }


    public function edit(Todo $todo) {
        /* $this->validate($request,[
            'todoName'=>'required|min:6',
            'todoDescription'=>'required'
        ]);  */
        //$todo=Todo::find($todo);
        return view('todos.edit')->with('todo',$todo);
    }

    
    public function update(Request $request,Todo $todo) {
        
    $this->validate($request,[
    'todoName'=>'required|min:6',
    'todoDescription'=>'required'
    ]);

    //$todo= Todo::find($todo);
    $todo->title=$request->get('todoName');
    $todo->description=$request->get('todoDescription');

    $todo->save();
    
    $request->session()->flash('success' ,'todo updated successfuly');
        
    return redirect('/todos');

    }
    
    public function destroy( Todo $todo ) {
        //$todo=Todo::find($todo);
        $todo->delete();
        
        session()->flash('success' ,'todo deleted successfuly');
        
        return redirect('/todos');
    }

    public function complete(Todo $todo) {
       
        $todo->completed=true;

        $todo->save();
        session()->flash('success' ,'todo completed successfuly');
        
        return redirect('/todos');
    }
    public function uncomplete(Todo $todo) {
      
        $todo->completed=false;

        $todo->save();
        session()->flash('success' ,'todo is incomplete');
        
        return redirect('/todos');
    }

}
