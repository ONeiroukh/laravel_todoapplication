<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    //
    //protected $table='my_todos';
    protected $attributes = [
        'completed' => false,
    ];

}
